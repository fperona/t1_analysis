% This class contains the data from a T1 file and defines the methods that
% can operate over it.
%
% Dependencies:
%   biexp_model.m
%   biexp_model2.m
%   doNORM.m
%   processSegmets.m
%   read_T1_file.m
%   save_xlsx.m
%   getT1curve.m
%   read_txt.m
%   read_bin.m
%   count_photons.m
%   find_start_stop.m
%   cellstr2vec.m
%   get_value.m
%
% Method:
%   T1curve(filepath) : Constructor, creates the object by reading a T1
%                       file.
%   calc_T1curve() : Calculates the T1 relaxation curve with the
%                    information stored in the 'T1curve' object.
%   fit_model(model,raw) : Find the curve that best fits the model defined
%                          by the input parameter 'model' (only 'biexponential'
%                          and 'exponential' are working now).
%                          model: 'biexponential' (default)
%                                 'biexponential2' (TEST: improved version of biexponential)
%                                 'exponential'   (Fit the data to a single exponential decay)
%                                 'singleNV'      (not implemented yet)
%                          raw: {true, false} if false it uses a normalised
%                          version of the relaxation curve. If true, uses
%                          the curve from the raw data.
%   plot_curve(type) : Plots the relaxation curve, the fitting curve and
%                      shows the T1 value in the figure.
%                      type: 'normalised' shows the normalised curve (default).
%                            'raw' shows the curve from the raw data.
%   process_all(plt) : Run, sequentially the methods calc_T1curve and
%                          fit_model. if 'plt' (plot) is 'true' then it
%                          also runs plot_curve.
%   getPLsignal(fromto) : Integrates all the runs into one sequence of
%                         pulses. The output 'sum_pl(i)' is the addition of
%                         all the photons detected at time 'i' (referenced
%                         to the begining of the current run).
%                         fromto: a vector [a b] defining the initial (a) and
%                                 last (b) run to process. Default, all the
%                                 runs.
%   getPulses(obj,fromto) : Chops the train of pulses produced by
%                           getPLsignal into isolated pulses.
%                           fromto: a vector [a b] defining the initial (a) and
%                                   last (b) run to process. Default, all the
%                                   runs.
%   calCorrection() : Calculates the 'correction' parameter from the integrated PL
%                     signal.
%   getSegments(segments,norm,plt) : Calculate the T1 constants spliting the 
%                                    dataset to 'segments' subsets.
%
%   getDynamics(win_size,shift_size) : calculates the T1 values over a subset 
%                                      of T1 runs, using the runing window
%                                      process.
%
%   cleanT1curve() : Remove outlayers from the T1 curve.
%   SNRon() : Evaluate the SNR of each run as the ratio between 
%             the mean value and the standard deviation of the PL 
%             when the laser is ON.
%   OnOffRatio() : Evaluate the quality of each run as the pulses on-off ratio.
%   calRunIntensity() : 
%   evalQArun() : 
%   setWhitelist(list) : Sets the runs to be used to calculate the T1 curve.
%   setDarktimesRange(range) : Select the darktimes used to build the curve.
%                              'range' contains the indexes of the first 
%                              and last darktime to use.
%   setModel(model) : Changes the fitting model to all the curves in an T1curve array.
%                     'model' is:
%                      'biexponential' (default)
%                      'biexponential2' (TEST: improved version of biexponential)
%   getName() : Print the name of all the files in the dataset.
%   getdarktimes() : Return the vector containing the darktimes used in the experiment.
%   getT1() : Return the calculated T1 value for each file.
%   getFitParameters() : Return the parameters resulting from the fitting.
%   getR2() : Return the Adj. R^2 value resulting for the fitting.
%   getGroup() : Return the group that the measurement belong.
%   getSample() : Return the sample names.
%   getMeasureNum() : Return the measurement number.
%   reset_curve() : Reset the curve and the calculations.
%   resetDarktimesRange() : Selects all the darktimes in the file for the analysis.
%   saveXlsx(file_path) : Save the T1 values [and curves] to an Excel file.
%                         The Excel file summarise the results of all the 
%                         T1 files in a T1_experiment array.

classdef T1curve < handle
    properties
        correction = NaN; % Value of the correction used to syncronise the triggers [us].
        read_width = NaN; % Size of the detection window [us].        
        model = "biexponential"; % Model used to fit the data and calculatethe T1 value.
        name = ''; % String. Name to show in legends and tables
        group = ''; % String. Name of the data set (group) which the data belongs.
        sample = ''; % String. Name of the sample (individual, timepoint, etc.).
        measure_num = 1; % Double, number of the measurment in a set of experiments.
        whitelist = []; % Set of runs to use in the analysis.
        QA_score = NaN;
    end
    
    properties
        darktimes = []; % Corresponding dark times of the T1_curve [us].
        darktimes_whitelist = [] % Range of darktimes of interest (indexes or boolean).
        T1_curve = []; % Integrated PL over the detection window of each reading pulse. Curve not normalised
        T1_curve_n = []; % Normalised T1 curve.
        T1_curve_f = []; % Fitted curve acording to the model used.
        T1 = NaN; % Value of the spin-lattice relaxation constant.
        PL_T1 = NaN; % Photoluminescence at T1.
        fit_param = []; % Array with all the calculated parameters used to fit the model.
        fit_error = NaN; % Norm of the residues of the fitting.
        repetitions = NaN; % Number of repetitions of a single run (one scanning of all the darktimes).
        pulse_width = NaN; % Size of the laser pulse in [us].
        t_int = []; % Vector with the time line of the integrated measurements.
        triggers_int = []; % Calculated triggers (mean value) for the integrated PL signal.
        PL_int = []; % Integrated PL signal, built from all the runs recorded in the experiment.
        pulses_int = []; % Isolated pulses from the integrated PL, each row has a pulse of pulse_width length.
        SNR_run = NaN; % Signal to Noise Ratio of each run.
        SNR = NaN; % Signal to Noise Ratio of the integrated PL signal.
        errors = []; % List of errors found in the analysis process.
        MinAdjR2 = 0.93; % (Not used now) Minimum value of the adjusted R^2 of the T1 curve fitting considered as acceptable.

        T1_data;    % Raw data from the T1 file.
    end
    
%     properties (Hidden, GetAccess = private, SetAccess=private)
%         T1_data
%     end
    
    
    methods
        %% Constructor
        function obj=T1curve(filepath)
            % Read the file and get the data as a cell array.
            obj.T1_data =  read_T1_file(filepath);
            
            % Fill the properties with the data.
            obj.read_width = obj.T1_data.read_width;
            obj.correction = obj.T1_data.correction + obj.T1_data.read_delay;
            obj.darktimes = obj.T1_data.darktimes;
            obj.darktimes_whitelist = 1:length(obj.darktimes);
            obj.repetitions = obj.T1_data.repetitions;
            obj.pulse_width = obj.T1_data.pulse_width;
            obj.T1_curve = obj.T1_data.PL_measured;
            obj.T1_curve_n = obj.T1_data.PL_measured;
            obj.T1_curve_f = obj.T1_data.PL_fit;
            obj.name = obj.T1_data.filename;
            obj.whitelist = 1:obj.T1_data.repetitions;
            obj.SNR_run = NaN(1, obj.repetitions);
            tmp = strsplit(obj.T1_data.filename,'_');
            if length(tmp)>1
                obj.measure_num = str2double(tmp(end));
                obj.group = obj.T1_data.filename(1:end-3);
            else
                obj.measure_num = 1;
                obj.group = obj.T1_data.filename;
            end
            
            % Release memory
            clear T1_data tmp
        end
        
        
        %% === Basic functions for T1-curve building and T1 calculation ===

        %% Calculate the relaxation curve 
        function calc_T1curve(obj)
            for i=1:numel(obj)
                % Calculate the raw curve
                data_f = obj(i).T1_data.timetags(obj(i).whitelist);
                trigg_f = obj(i).T1_data.triggers(obj(i).whitelist);
                obj(i).T1_curve = getT1curve(trigg_f, data_f, obj(i).correction, obj(i).read_width);
    
                % Normalyse the curve
                obj(i).T1_curve_n = doNORM(obj(i).T1_curve,'tail');
            end
        end
        
        
        %% Apply the model to calculate the T1 value
        function fit_model(objList,model,raw)
            for obj = objList(:)'
                fprintf("%s: \t", obj.name)
                curve = obj.T1_curve_n(obj.darktimes_whitelist);
                dtimes = obj.darktimes(obj.darktimes_whitelist);
                if exist('raw','var') ~= 0
                    if raw
                        curve = obj.T1_curve(obj.darktimes_whitelist);
                    end
                end
                
                if exist('model','var') == 0
                    model = obj.model;
                else
                    obj.model = model;
                end
                
                switch model
                    case 'biexponential'
                        [obj.T1_curve_f, obj.fit_param, obj.PL_T1, obj.QA_score, ~, ~, obj.fit_error] = biexp_model(dtimes,curve);
                        obj.T1 = max(obj.fit_param([3 5])); % T1 is the longest time constant.
                    case 'biexponential2'
                        [obj.T1_curve_f, obj.fit_param, obj.PL_T1, obj.QA_score, ~, ~, obj.fit_error] = biexp_model2(dtimes,curve);
                        obj.T1 = obj.fit_param(4);
                    case 'exponential'
                        [obj.T1_curve_f, obj.fit_param, obj.PL_T1, obj.QA_score, ~, ~, obj.fit_error] = singexp_model(dtimes,curve);
                        obj.T1 = obj.fit_param(2);
                    case 'singleNV'
                        disp('Not implemented yet.')
                    otherwise
                        disp('Select between biexponential, exponential or singleNV')
                end
            end
        end
        
        
        %% Plot the T1 and fitting curves.
        function plot_curve(obj,type)
            figA = figure;

            % Get the current colour palette (line colors)
            co = get(groot,'DefaultAxesColorOrder');

            % Set the same color to the data points and the fitting curve.
            co = reshape([co(:) co(:)]',2*size(co,1), []); % Assign the same color to two consecutive lines
            set(figA,'defaultAxesColorOrder',co); % Set the new line color array to the axes.

            % Plot the lines
            if exist('type','var') == 0
                type = 'normalised';
            end
            switch type
                case 'raw'
                    semilogx(obj.darktimes,obj.T1_curve,'.');
                    legend(obj.name);
                otherwise
                    semilogx(obj.darktimes(obj.darktimes_whitelist),obj.T1_curve_n(obj.darktimes_whitelist),'.',obj.darktimes(obj.darktimes_whitelist),obj.T1_curve_f,'-',obj.T1,obj.PL_T1,'pr');
                    val = sprintf('T1: %.2f [us]', obj.T1);
                    legend({obj.name, 'Fitted curve', val});
            end
            l=legend('show');
            set(l, 'Interpreter', 'none');
            legend boxoff;
            grid on;
            title('Spin-lattice relaxation curve');
            xlabel('Dark time [us]');
            ylabel('Intensity [a.u.]');
            
        end
        
      
        %% Calculate the curve, fit the model and plot them in one shoot.
        function process_all(obj,plt)
            % Check the optional parameter
            if exist('plt','var') == 0
                plt = false;
            end            

            q_files = length(obj);
            for i=1:q_files
                obj(i).calc_T1curve
                fprintf("%d\t", i)
                obj(i).fit_model;
                if plt
                    obj(i).plot_curve;
                end 
            end
        end
    

        %% Build the pulsed PL signal from a set of measures
        function [t, triggers, sum_pl] = getPLsignal(obj,fromto)
            % Check the optional parameter
            if exist('fromto','var') == 0
                fromto = [1 obj.repetitions];
            end
            
            % Build the time variable
            t = 1:max(cellfun(@max,obj.T1_data.timetags(fromto(1):fromto(2),:))); % [counter cycles (10ns)]
            
            % Count the number of photons detected at the same time after started the experiment.
            sum_pl = zeros(1,length(t));
            for i=fromto(1):fromto(2)
                sum_pl(obj.T1_data.timetags{i}) = sum_pl(obj.T1_data.timetags{i}) + 1;
            end
            
            % Average the trigger position
            triggers = [1 mean(cell2mat(obj.T1_data.triggers))] + obj.correction*100; % [index = 10 ns]
            triggers = round(triggers);
            
            % Store the result in the object
            obj.t_int = t;
            obj.triggers_int = triggers;
            obj.PL_int = sum_pl;
        end
        
        %% Build the pulsed PL signal from a set of measures, applying the 'whitelist'.
        function [t, triggers, sum_pl] = getPLsignal2(obj)
            % Translate the whitelist to run number in the case it is given
            % as a boolean vector.
            runs = 1:obj.repetitions;
            runs = runs(obj.whitelist(:));

            % Build the time variable
            t = 1:max(cellfun(@max,obj.T1_data.timetags(runs,:))); % [counter cycles (10ns)]
            
            % Count the number of photons detected at the same time after started the experiment.
            sum_pl = zeros(1,length(t));
            for i=runs
                sum_pl(obj.T1_data.timetags{i}) = sum_pl(obj.T1_data.timetags{i}) + 1;
            end
            
            % Average the trigger position
            triggers = [1 mean(cell2mat(obj.T1_data.triggers))] + obj.correction*100; % [index = 10 ns]
            triggers = round(triggers);
            
            % Store the result in the object
            obj.t_int = t;
            obj.triggers_int = triggers;
            obj.PL_int = sum_pl;
        end
        

        %% Separate the pulses from getPLsignal.
        function pulses = getPulses(obj,fromto)
            % Check the optional parameter
            if exist('fromto','var') == 0
                fromto = [1 obj.repetitions];
            end
            
            % Number of pulses in one run
            q_pulses = length(obj.darktimes);
            
            % Convert the pulse width [us] to index
            p_width = obj.pulse_width * 100; % samples
            
            % Get the train of pulses
            [~, triggers, sum_pl] = getPLsignal(obj,fromto);
            
            pulses = zeros(q_pulses,p_width);
            for i = 1:q_pulses
                pulses(i,:) = sum_pl(triggers(i):(triggers(i) + p_width - 1));
            end
            
            % Store the result in the object
            obj.pulses_int = pulses;
        end
        

        %% calCorrection :  Calculates the average difference between the 
        %                   triggers and the rising edge of the detected
        %                   fluorescent pulses.
        function calCorrection(objList)
            for obj = objList(:)'
                correction_bkp = obj.correction;
                obj.correction = 0;
                [t, ~, pl] = obj.getPLsignal;
                [p_width,rise_edges,~, ~]=pulsewidth(pl,t);
                if (length(rise_edges) == length(obj.triggers_int))
                    correction_new = mean(abs(rise_edges - obj.triggers_int)) * 1e-2; % μs
                    p_width = mean(p_width(2:end)) * 1e-2; % μs
                    if (p_width > 0.9*obj.pulse_width && p_width < 1.1*obj.pulse_width)
                        obj.correction = correction_new;
                    else
                        msg = "ERROR: calculation of the 'correction' parameter failed.\n";
                        fprintf(2,msg);
                        obj.errors = [obj.errors {msg}];
                        obj.correction = correction_bkp;
                    end
                else
                        msg = "ERROR: calculation of the 'correction' parameter failed.\n";
                        fprintf(2,msg);
                        obj.errors = [obj.errors {msg}];
                    obj.correction = correction_bkp;
                end
            end
        end


        %% ========== Data segmentation and rolling window =============

        %% Calculate the T1 constants spliting the dataset into 'segments' subsets.
        function T1s = getSegments(obj,segments,norm,plt)
            % Check the optional parameter
            if exist('norm','var') == 0
                norm = false;
            end
            if exist('plt','var') == 0
                plt = false;
            end
            
            % Check if the amount of repetitions can be divided by the
            % segment size
            q_run_seg = floor(obj.T1_data.repetitions/segments);               
            remanent = obj.T1_data.repetitions - q_run_seg*segments;
            if remanent == 0
                % Set the amount of runs in each segment
                q_run_seg = q_run_seg * ones(1,segments);
            else
                % Set the amount of runs in each segment
                q_run_seg = [q_run_seg * ones(1,segments-1), q_run_seg + remanent];
            end
            
            % Find the position at which each segment starts and stops.
            seg_start = zeros(1,segments);
            seg_stop = zeros(1,segments);
            seg_stop_ = 0;
            for i=1:segments
                seg_start(i) = seg_stop_ + 1;
                seg_stop_ = sum(q_run_seg(1:i));
                seg_stop(i) = seg_stop_;
            end
            fromto(:,1) = seg_start;
            fromto(:,2) = seg_stop;
            
            % Segment the data and calculate the curve.
            curve = zeros(segments,length(obj.darktimes));
            for i=1:segments
                seg_timetags = obj.T1_data.timetags(seg_start(i):seg_stop(i));
                seg_triggers = obj.T1_data.triggers(seg_start(i):seg_stop(i));
                tmp = getT1curve(seg_triggers,seg_timetags,obj.correction,obj.read_width);
                curve(i,:) = tmp;
            end
            
            % Calculate the T1 value of each curve
            T1curve_f = zeros(size(curve));
            fitparam = zeros(segments, 5);
            T1s = zeros(segments,1);
            PLT1 = zeros(segments,1);
            fiterror = zeros(segments,1);
            for i=1:segments 
                [T1curve_f(i,:), fitparam(i,:), PLT1(i), fiterror(i)] = biexp_model(obj.darktimes,curve(i,:));
                T1s(i) = max(fitparam(i,[3 5])); % T1 is the longest time constant.
            end
            
            %% Normalyse before ploting if norm==true.
            if norm
                norm_vals = T1curve_f(:,end);
                curve_n = curve./norm_vals;
                curve_f_n = T1curve_f./norm_vals;
                curve = curve_n;
                T1curve_f = curve_f_n;
                PLT1 = PLT1./norm_vals;
            end
            
            %% Show the time constants
            Tc_tab = table((1:segments)',fromto,T1s,'VariableNames',{'Segment','From_To','Tc'});
            disp(Tc_tab);
            
            %% Plot all the curves
            if plt
                fig_seg = figure;

                % Get the current colour palette (line colors)
                co = get(groot,'DefaultAxesColorOrder');

                % Flip the yellow color with the green
                co([3 5],:) = co([5 3],:);

                % Set the same color to the data points and the fitting curve.
                co = reshape([co(:) co(:)]',2*size(co,1), []); % Assign the same color to two consecutive lines.
                set(fig_seg,'defaultAxesColorOrder',co); % Set the new line color array to the axes.

                for i=1:segments
                    semilogx(obj.darktimes,curve(i,:),'.',obj.darktimes,T1curve_f(i,:),'-',T1s(i),PLT1(i),'pr');
                    % Set the filename to show in the legend.
                    text = sprintf('Segment %d',i);
                    fig_seg.Children.Children(3).DisplayName = text;
                    % Show only the data line in the legend.
                    fig_seg.Children.Children(1).Annotation.LegendInformation.IconDisplayStyle = 'off';
                    fig_seg.Children.Children(2).Annotation.LegendInformation.IconDisplayStyle = 'off';
                    % Increase the size of the point.
                    fig_seg.Children.Children(3).MarkerSize = 12;
                    hold on
                end
                grid on;
                title('Spin-lattice relaxation curve');
                xlabel('Dark time [us]');
                ylabel('Intensity [a.u.]');            
                legend boxoff;

                % Plot the constants in a separate figure
                pos_x = mean(fromto,2);
                figure;
                bar(pos_x,T1s,1);
                xlim([0 obj.T1_data.repetitions])
                grid on;
                title('Segments T_1 values');
                xlabel('Run #');
                ylabel('T_1 constant [us]');
            end
        end
        
        
        %% getDynamics: calculates the T1 values over a subset of T1 runs.
        %  The inputs are:
        %   win_size: quantity of runs from which to calculate the constant.
        %   shift_size: The amount of runs to move forward to calculate a
        %   T1 constant (step size).
        function [T1s, error, fitparam] = getDynamics(obj,win_size,shift_size)
            % Find the position at which each segment starts and stops.
            fromto(:,1) = 1:shift_size:(obj.T1_data.repetitions - win_size + 1);
            fromto(:,2) = win_size:shift_size:obj.T1_data.repetitions;
            
            [~, T1s, ~, error, fitparam] = processSegmets(obj.T1_data.timetags,obj.T1_data.triggers,obj.darktimes,fromto,obj.correction,obj.read_width);
        end
        
        %% =============== Cleaning and filtering T1-curves ===============

        %% cleanT1curve: Remove outlayers from the T1 curve.
        %                Outlayers are determined using the Standard score
        %                (z-score). A point is eliminated if the sum of its
        %                derivatives (with the previous point and with the next
        %                point) is one standard deviation higher than the
        %                mean.
        function cleanT1curve(obj)
            for i=1:numel(obj)
                ddt = diff(obj(i).darktimes);
                dy = diff(obj(i).T1_curve_n);
                deri = abs(dy./ddt);
                meas = [deri deri(end)] + [deri(1) deri];
                z = (meas - mean(meas))/std(meas);
                mask = z <= 1;
                obj(i).darktimes_whitelist = mask;
            end
        end


        %% ================= Quality measurement functions ================
        % This methods evaluate the quality of the data recorded (in
        % different ways) and the quality of the T1 calculations.

        %% SNRon :      Evaluate the SNR of each run as the ratio 
        %               between the mean value and the standard deviation 
        %               of the PL when the laser is ON.
        function SNRon(objList)
            for obj = objList(:)'
                pw = obj.pulse_width * 100; % pulse_width is in us, pw in counts
                for i = 1:obj.repetitions
                    % Get segments where the laser is on
                    on_starts = obj.T1_data.triggers{i} + obj.correction * 100 - 1;
                    on_ends = on_starts + pw;
                    
                    % Calculate the expected value and standard deviation of the 
                    % number of photons detected in a laser pulse
                    photons_per_pulse = count_photons(obj.T1_data.timetags{i}, on_starts, on_ends);
                    S = mean(photons_per_pulse);
                    N = std(photons_per_pulse);

                    % Calculate SNR
                    obj.SNR_run(i) = S/N;
                end
            end
        end
      

        %% OnOffRatio:      Evaluate the SNR of each run as the pulses 
        %                   on-off ratio.
        function OnOffRatio(objList)
            t_sampling = 100; % counts / us. 
            for obj = objList(:)'
                pw = obj.pulse_width * t_sampling; % pulse_width is in us, pw in counts
                for i = 1:obj.repetitions
                    % Get segments where the laser is on
                    on_starts = obj.T1_data.triggers{i} + obj.correction * t_sampling - 1;
                    on_ends = on_starts + pw;

                    % Get segments where the laser is off
                    off_ends = on_starts - 1;
                    off_starts = off_ends - obj.darktimes * t_sampling;
                    
                    % Count the number of photons detected in the ON segments
                    photons_on = count_photons(obj.T1_data.timetags{i}, on_starts, on_ends);
                    photons_on = sum(photons_on) / (length(photons_on)*obj.pulse_width); % photons/us

                    % Count the number of photons detected in the OFF segments
                    photons_off = count_photons(obj.T1_data.timetags{i}, off_starts, off_ends);
                    photons_off = sum(photons_off) / sum(obj.darktimes);  % photons/us
                    
                    % Calculate SNR
                    obj.SNR_run(i) = photons_on/photons_off;
                end
            end
        end
   
        %% SNR_PL:  SNR from the integrated PL. As on-off ratio.
        function snr = SNR_PL(objList)
            t_sampling = 100; % counts / us.
            for obj = objList(:)'
                [t, trig, pl] = getPLsignal2(obj);
                pw = obj.pulse_width * t_sampling; % pulse_width is in us, pw in counts
                q_pulses = length(trig) - 1;

                % Get segments where the laser is on
                on_starts = trig(2:end);
                on_ends = on_starts + pw;

                % Get segments where the laser is off
                off_ends = on_starts - 1;
                off_starts = off_ends - obj.darktimes * t_sampling;

                photons_on = zeros(1, q_pulses);
                photons_off = zeros(1, q_pulses);
                for i=1:length(on_starts)
                    % Count the number of photons detected in the ON segments
                    photons_on(i) = sum(pl((t >= on_starts(i)) & (t <= on_ends(i))));

                    % Count the number of photons detected in the OFF segments
                    photons_off(i) = sum(pl((t > off_starts(i)) & (t <= off_ends(i))));
                end
                
                % Total rate of photon when laser is ON (photons/us)
                phs_on = sum(photons_on)/(q_pulses * obj.pulse_width);

                % Total rate of photon when laser is OFF (photons/us)
                phs_off = sum(photons_off)/sum(obj.darktimes);

                % Calculate SNR
                obj.SNR = phs_on/phs_off;
            end
            snr = [objList.SNR]';
        end
        
        
        %% calRunIntensity : Calculates the total number of photons detected on each run.
        function qPh = calRunIntensity(objList)
            qf = numel(objList);
            qPh = cell(qf,1);
            for i = 1:qf
                tmp = arrayfun(@(x) size(x{:},2), objList(i).T1_data.timetags);
                qPh(i) = {tmp};
            end
        end


        %% evalQArun : Calculates the overall quality of each run.
        function qa_table = evalQArun(objList)
            qph_thres = 100; % Minimum number of photons expected in one run
            runs_thres = 0.51; % Minimum rate of runs with detections over qph_thres
            snr_thres = 2; % Minimum SNR of the integrated PL

            qf = numel(objList);
            objList.setWhitelist(); % Reset whitelist.
             % Total detected photons on each run of all the files.
            qph = calRunIntensity(objList);
            qa_qph = cell(qf,1);
            qa_runs = zeros(qf,1);
            snr_raw = zeros(qf,1);
            for i = 1:qf
                qruns = objList(i).repetitions;
                snr_raw(i) = SNR_PL(objList(i));
                qa_qph(i) = {qph{i} > qph_thres};
                objList(i).setWhitelist(qa_qph{i});
                qa_runs(i) = sum(qa_qph{i})/qruns; % Rate of usefull runs in the file
                objList(i).QA_score = qa_runs(i) >= runs_thres;
            end

            qa = [objList.QA_score]';
            files = [objList.name]';
            snr = SNR_PL(objList);
            qa = qa & (snr >= snr_thres);
            qa_table = table(files, snr_raw, qa_runs*100, snr, qa , 'VariableNames', {'File', 'SNR raw','Good Runs %', 'SNR whitelist','Pass QA'});
        end


        %% ========================== SET functions =======================
        % This functions are used to set the parameters for all the elements in
        % an array of T1curve.
        
        
        %% setWhitelist: Sets the runs to be used to calculate the T1 curve.
        %                If the parameter 'list' is empty, then it resets the whitelist to 1:repetitions
        function setWhitelist(objList, runlist)
            for obj = objList(:)'
                runs = 1:obj.repetitions;
                if exist('runlist','var') == 0
                    runlist = runs;
                end
                
                % Translate the whitelist to run number in the case it is given
                % as a boolean vector.
                runs = runs(runlist);
                obj.whitelist = runs;
            end
        end


        %% setDarktimesRange: Select the darktimes used to build the curve.
        %                     range contains the indexes of the first and last
        %                     darktime to use.
        function setDarktimesRange(obj, range)
            for i=1:numel(obj)
                obj(i).darktimes_whitelist = range;
            end
        end


        %% setModel: Changes the fitting model to all the curves in the array.
        function setModel(obj, model)
            for i=1:numel(obj)
                obj(i).model = model;
            end
        end


        %% ========================== GET functions ==========================
        % This functions are used to retrieve the value of a parameter from
        % all the elements in an array of T1curve.


        %% Print the name of all the files in the dataset
        function names = getName(obj)
            names = [obj.name]';
            file = [1:length(names)]';
            disp(table(file, names));
        end
        

        %% Return the vector containing the darktimes used in the experiment.
        function dt = getdarktimes(obj)
            dt = obj.darktimes;
        end


        %% getT1 : Return the calculated T1 value for each file in the input array
        function t1s = getT1(obj)
            t1s = [obj.T1]';
        end


        %% getFitParameters: Return the parameters resulting from the fitting.
        function fitParameters = getFitParameters(obj)
            fitParameters = cell2mat({obj.fit_param}');
        end


        %% getR2 : Return the Adj. R^2 value resulting for the fitting.
        function r2s = getR2(obj)
            r2s = [obj.fit_error]';
        end


        %% getGroup : Return the group that the measurement belong
        function groups = getGroup(obj)
            groups = [obj.group]';
        end


        %% getSample : Return the sample names.
        function samples = getSample(obj)
            samples = [obj.sample]';
        end


        %% getMeasureNum : Return the measurement numbers.
        function mnum = getMeasureNum(obj)
            mnum = [obj.measure_num]';
        end


        %% ======================== RESET functions =======================
        % This functions are used to reset the value of a parameter to
        % the default to all the elements in the 'obj' array.

        %% Reset the curve and the calculations.
        function reset_curve(obj)
            for i=1:numel(obj)
                obj(i).T1_curve = []; % Integrated PL over the detection window of each reading pulse. Curve not normalised
                obj(i).T1_curve_n = []; % Normalised T1 curve.
                obj(i).T1_curve_f = []; % Fitted curve acording to the model used.
                obj(i).T1 = NaN; % Value of the spin - lattice relaxation constant.
                obj(i).PL_T1 = NaN; % Photoluminescence at T1.
                obj(i).fit_param = []; % Array with all the calculated parameters used to fit the model.
                obj(i).fit_error = NaN; % Norm of the residues of the fitting.
            end
        end

        
        %% resetDarktimesRange: Selects all the darktimes in the file for 
        %                       the analysis.
        function resetDarktimesRange(obj)
            for i=1:numel(obj)
                obj(i).darktimes_whitelist = 1:length(obj(i).darktimes);
            end
        end


        %% ===================== Save results =============================

        %% saveXlsx:        Save the T1 values [and curves] to an Excel file.
        %                   The Excel file summarise the results of all the 
        %                   T1 files in a T1_experiment array.
        function saveXlsx(obj, file_path)
            save_xlsx(obj, file_path);
        end


    end
end