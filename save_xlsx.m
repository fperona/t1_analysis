function save_xlsx(T1_exp, filepath)
    fname = ['\Results_T1_' T1_exp(1).model '.xlsx'] ;
    docnam = strcat(filepath, fname);

    expName = {T1_exp.name}';
    T1s = cell2mat({T1_exp.T1}');
    % y_TC = cell2mat({T1_exp.PL_T1}');
    fit_model = {T1_exp.model}';
    QA_fits = cell2mat({T1_exp.QA_score}');
    adjR2 = cell2mat({T1_exp.fit_error}');
    T = table(expName, T1s, fit_model, QA_fits, adjR2, 'VariableNames', {'Filename', 'T1', ' Model', 'Fitting Quality', 'Adj. R^2'});
    writetable(T, docnam, 'Sheet', 'T1s')

    darktimes = unique([T1_exp.darktimes])';
    
    q_exp = numel(T1_exp);
    curves = NaN(length(darktimes), 2*q_exp);
    vnames = strings(1, 1 + 2*q_exp);
    vnames(1) = 'Darktime';
    q_dt = zeros(q_exp,1);
    filt = false(q_exp,1);
    for i=1:q_exp
        icol = 2*i-1;
        ind = find(ismember(darktimes, T1_exp(i).darktimes(T1_exp(i).darktimes_whitelist))');
        curves(ind,icol) = T1_exp(i).T1_curve_n(T1_exp(i).darktimes_whitelist);
        curves(ind,icol+1) = T1_exp(i).T1_curve_f;
        vnames(icol+1) = T1_exp(i).name+"_n";
        vnames(icol+2) = T1_exp(i).name+"_f";

        q_dt(i) = numel(T1_exp(i).darktimes);
        filt(i) = ~(q_dt(i) == nnz(T1_exp(i).darktimes_whitelist));
    end

    T = array2table([darktimes curves],'VariableNames', vnames);
    writetable(T, docnam, 'Sheet', 'Curves')
    
    rep = cell2mat({T1_exp.repetitions}');
    rwidth = cell2mat({T1_exp.read_width}');
    correc =  cell2mat({T1_exp.correction}');
    pwidth =  cell2mat({T1_exp.pulse_width}');
    T = table(expName, q_dt, rep, pwidth, rwidth, correc, filt, 'VariableNames', {'Filename', 'N darktimes', ' Repetitions', 'Pulse width μs', 'Read width μs', 'Correction μs', 'Filtered'});
    writetable(T, docnam, 'Sheet', 'Parameters')

end