%% Folder to analyse
% Path to the folder where the data files are.
folder = 'D:\Data\Willem\T1 Elina\20250124 SaRN4220 Vancomycine\SaRN4220blank_03_P01';


%% Parameters
MODEL = 'biexponential'; % Set the model to fit the data.
FIT     = true; % TRUE or FALSE; turns on or off whether all the data has to be fitted with the specified models
SAVERES = true; % Save the resutls in an Excel file

%% loading the files
if exist('FILES_LOADED', 'var')==0
    [files, ~] = findT1files(folder);
    T1_experiment = arrayfun(@(f) T1curve(f), files);
    
    clear files % clear the variables
    FILES_LOADED = true; % avoid to load the files if the script is ran again
else
    clear Tcseg Tcseg_o T1s;
end

%% Fit the models
if FIT == true
    T1_experiment.setModel(MODEL);
    T1_experiment.process_all();
end

%% Save the results in an Excel file in the same data folder
if SAVERES == true
    T1_experiment.saveXlsx(folder);
end
