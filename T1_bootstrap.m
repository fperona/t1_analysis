function [mod_biL, mod_biW, mod_si, mod_sr] = T1_bootstrap(name, timetags, triggers, darktimes, rwid, corr, model, NORM, bootsam, filepath, outrem)
%% this function creates a bootstrap for the timetags and triggers that are applied to the system.
% To speed up the process the program writes away a lot of .mat files, so
% when running this script enough memory has to be free.
% The high number of inputs allows to script to be used more freely and
% omitting data that is not needed for the bootstrap itself.
    
%% make the folder for the bootstrap samples

bootnam = strcat(filepath, name,'_bootsam\');
if isfolder(bootnam) ==0
    mkdir (bootnam)
end

%%  Create the bootsrap samples and calculate the T1

rep = length(timetags);

bootcurvfol = strcat(bootnam,name, '_bootcurve\');
if isfolder(bootcurvfol) ==0
    mkdir(bootcurvfol);
end


parfor j = 1:bootsam
    rng('shuffle');
    bootsub = randi(rep,[1 rep]);
    timet = timetags(bootsub);
    trigg = triggers(bootsub);
    bootsub = bootsub';
    tmp = getT1curve(trigg,timet,corr,rwid);
    boot = strcat(bootnam,num2str(j),'.mat');
    parsave(boot,bootsub);
    
    if strcmp(model,'biexp') || strcmp(model,'all')
        if NORM == true
            tmp = doNORM(tmp,'tail');
        end
        [T1curve_bi, fitparam_bi(j,:), ~, ~] = biexp_model(darktimes,tmp);
        bootcurve = strcat(bootcurvfol,name,'_biexp_curve_',num2str(j),'.mat');
        parsave(bootcurve,T1curve_bi);
    end
    
    if strcmp(model,'siexp') || strcmp(model,'all')
        if NORM == true
            tmp = doNORM(tmp,'tail')
        end
        [T1curve_si, fitparam_si(j,:), ~, ~] = singexp_model(darktimes,tmp);
        bootcurve = strcat(bootcurvfol,name,'_siexp_curve_',num2str(j),'.mat');
        parsave(bootcurve, T1curve_si);
    end
    
    if strcmp(model,'srexp') || strcmp(model,'all')
        tmp = doNORM(tmp,'stretch');
        [T1curve_sr, fitparam_sr(j,:), ~, ~] = stretch_model(darktimes,tmp);
        bootcurve = strcat(bootcurvfol,name,'_srexp_curve_',num2str(j),'.mat');
        parsave(bootcurve, T1curve_sr);
    end
end

if strcmp(model,'biexp') || strcmp(model,'all')
    T1L = fitparam_bi(:,5);
    for k=1:length(T1L)
        if fitparam_bi(k,2) > fitparam_bi(k,4)
            T1W(k,1) = fitparam_bi(k,3);
        else
            T1W(k,1) = fitparam_bi(k,5);
        end
    end
    T1_biL = strcat(filepath,name,'_boot_biexp-long_T1-list.xlsx');
    T = table(T1L, 'VariableNames',{'T1_long'});
    writetable(T, T1_biL)
    T1_biW = strcat(filepath,name,'_boot_biexp-weight_T1-list.xlsx');
    Y = table(T1W, 'VariableNames',{'T1_weight'});
    writetable(Y,T1_biW)
    clear T1_biL T T1_biW Y
end

if strcmp(model,'siexp') || strcmp(model,'all')
    T1S = fitparam_si(:,3);
    T1_si = strcat(filepath,name, '_boot_siexp_T1-list.xlsx');
    S = table(T1S, 'VariableNames', {'T1_siexp'});
    writetable(S, T1_si)
    clear S T1_si
end

if strcmp(model,'srexp') || strcmp(model,'all')
    T1R = fitparam_sr(:,3);
    T1_sr = strcat(filepath, name, '_boot_srexp_T1-list.xlsx');
    R = table(T1R, 'VariableNames', {'T1_srexp'});
    writetable(R, T1_sr)
    clear R T1_sr
end

clear bootsub tmp bootcurve T1curve_bi T1curve_si T1curve_sr trigg timet

%% create the T1 distribution based on KDE

min95 = 0.025;
max95 = 0.975;

if strcmp(model,'biexp') || strcmp(model,'all')
    maxhis_biL = max(T1L);
    meanhis_biL = mean(T1L);
    if maxhis_biL > (meanhis_biL*5)
        maxhis_biL = meanhis_biL*5;
    end
    [~, density, xmesh, cdf] = kde (T1L, 2^12, 0, maxhis_biL);
    x = find(density == max(density), 1, 'first');
    maxx_bi = xmesh(x);
    Y = [xmesh' density];
    writetable(table(Y), char(strcat(filepath,name,'_biexp-long_kde.xlsx')));
    min_biL = find(cdf >= min95, 1, 'first');
    min95_biL = xmesh(min_biL);
    max_biL = find(cdf >= max95, 1, 'first');
    max95_biL = xmesh(max_biL);
    mod_biL = [maxx_bi min95_biL max95_biL];
    writetable(table(mod_biL, 'VariableNames', {'T1_mode'}), char(strcat(filepath,name,'_biexp-long_kde_mode.xlsx')))
    clear cdf
    
    maxhis_biW = max(T1W);
    meanhis_biW = mean(T1W);
    if maxhis_biW > (meanhis_biW*5)
        maxhis_biW = meanhis_biW*5;
    end
    [~, density, xmesh, cdf] = kde (T1W, 2^12 ,0 , maxhis_biW);
    y = find(density == max(density), 1, 'first');
    maxx_biw = xmesh(y);
    Z = [xmesh' density];
    writetable(table(Z), char(strcat(filepath, name, '_biexp-weight_kde.xlsx')));
    min_biW = find(cdf >= min95, 1, 'first');
    min95_biW = xmesh(min_biW);
    max_biW = find(cdf >= max95, 1, 'first');
    max95_biW = xmesh(max_biW);
    mod_biW = [maxx_biw min95_biW max95_biW];
    writetable(table(mod_biW, 'VariableNames', {'T1_mode'}), char(strcat(filepath,name,'_biexp-weight_kde_mode.xlsx')))
    
    clear maxhis_biL maxhis_biW meanhis_biL meanhis_biW x maxx_bi U Z y maxx_biw Z Q density xmesh cdf
end

if strcmp(model,'siexp') || strcmp(model,'all')
    maxhis_si = max(T1S);
    meanhis_si = mean(T1S);
    if maxhis_si > (meanhis_si*5)
        maxhis_si = meanhis_si*5;
    end
    [~, density, xmesh, cdf] = kde(T1S, 2^12, 0, maxhis_si);
    x = find(density == max(density), 1, 'first');
    maxx_si = xmesh(x);
    U = [xmesh' density];
    writetable(table(U), char(strcat(filepath, name, '_siexp_kde.xlsx')))
    min_si = find(cdf >= min95, 1, 'first');
    min95_si = xmesh(min_si);
    max_si = find(cdf >= max95, 1, 'first');
    max95_si = xmesh(max_si);
    mod_si = [maxx_si min95_si max95_si];
    writetable(table(mod_si,'VariableNames', {'T1_mode'}), char(strcat(filepath,name,'_siexp_kde_mode.xlsx')))
    clear maxhis_si meanhis_si x density xmesh  maxx_si U P cdf
end

if strcmp(model, 'srexp') || strcmp(model, 'all')
    maxhis_sr = max(T1R);
    meanhis_sr = mean(T1R);
    if maxhis_sr > (meanhis_sr*5)
        maxhis_sr = meanhis_sr*5;
    end
    [~, density, xmesh, cdf] = kde(T1R, 2^12, 0, maxhis_sr);
    x = find(density == max(density), 1, 'first');
    maxx_sr = xmesh(x);
    A = [xmesh', density];
    writetable(table(A), char(strcat(filepath, name, '_srexp_kde.xlsx')))
    min_sr = find(cdf >= min95, 1, 'first');
    min95_sr = xmesh(min_sr);
    max_sr = find(cdf >= max95, 1, 'first');
    max95_sr = xmesh(max_sr);
    mod_sr = [maxx_sr min95_sr max95_sr];
    writetable(table(mod_sr,'VariableNames', {'T1_mode'}), char(strcat(filepath,name,'_srexp_kde_mode.xlsx')))
    clear maxhis_sr meanhis_sr density xmesh x maxx_sr A S cdf
end

%% create the samples without outliers
if outrem == true
    if strcmp(model, 'biexp') || strcmp(model, 'all')
        lbo = prctile(T1L, 25) - 1.5*iqr(T1L);
        ubo = prctile(T1L, 75) + 1.5*iqr(T1L);
        T1Lno = T1L(T1L > lbo & T1L < ubo);
        S = table(T1Lno, 'VariableNames', {'T1_biexp_long'});
        docnam2 = strcat(filepath, name, '_T1-list_biexp-long_no-outlier.xlsx');
        writetable(S,docnam2)
        maxhis_biL = max(T1Lno);
        meanhis_biL = min(T1Lno);
        if maxhis_biL > (meanhis_biL*5)
            maxhis_biL = meanhis_biL*5;
        end
        [~, density, xmesh, cdf] = kde(T1Lno, 2^12, 0, maxhis_biL);
        x = find(density == max(density), 1, 'first');
        maxx_bi = xmesh(x);
        A = [xmesh', density];
        writetable(table(A), char(strcat(filepath,name,'_biexp-long_kde_no-outlier.xlsx')));
        min_bi = find(cdf >= min95, 1, 'first');
        min95_bi = xmesh(min_bi);
        max_bi = find(cdf >= max95, 1, 'first');
        max95_bi = xmesh(max_bi);
        mod_bi_no = [maxx_bi min95_bi max95_bi];
        writetable(table(mod_bi_no, 'VariableNames', {'T1_mode'}), char(strcat(filepath,name,'_biexp-long_kde_mode_no-outlier.xlsx')))
        clear lbo ubo T1Lno S docnam2 maxhis_biL meanhis_biL density xmesh cdf x maxx_bi A min_bi min95_bi max_bi max95_bi mod_bi_no
        
        lbo = prctile(T1W, 25) - 1.5*iqr(T1W);
        ubo = prctile(T1W, 25) + 1.5*iqr(T1W);
        T1Wno = T1W(T1W > lbo & T1W < ubo);
        S = table(T1Wno, 'VariableNames', {'T1_biexp_weight'});
        docnam2 = strcat(filepath, name, '_T1-list_biexp-weight_no-outlier.xlsx');
        writetable(S,docnam2)
        maxhis_biW = max(T1Wno);
        meanhis_biW = mean(T1Wno);
        if maxhis_biW > (meanhis_biW*5)
            maxhis_biW = meanhis_biW*5;
        end
        [~, density, xmesh, cdf] = kde(T1Wno, 2^12, 0, maxhis_biW);
        x = find(density == max(density), 1, 'first');
        maxx_bi = xmesh(x);
        A = [xmesh', density];
        writetable(table(A), char(strcat(filepath, name, '_biexp-weight_kde_no-outlier.xlsx')))
        min_bi = find(cdf >= min95, 1, 'first');
        min95_bi = xmesh(min_bi);
        max_bi = find(cdf >= max95, 1, 'first');
        max95_bi = xmesh(max_bi);
        mod_bi_no = [maxx_bi min95_bi max95_bi];
        writetable(table(mod_bi_no, 'VariableNames', {'T1_mode'}), char(strcat(filepath, name, '_biexp-weight_kde_mode_no-outlier.xlsx')))
        clear lbo ubo T1Wno S docnam2 maxhis_biW meanhis_biW density xmesh cdf x maxx_bi A min_bi min95_bi max_bi max95_bi mod_bi_no
    end
    if strcmp(model, 'siexp') || strcmp(model, 'all')
        lbo = prctile(T1S, 25) - 1.5*iqr(T1S);
        ubo = prctile(T1S, 75) + 1.5*iqr(T1S);
        T1Sno = T1S(T1S > lbo & T1S < ubo);
        S = table(T1Sno, 'VariableNames', {'T1_siexp'});
        docnam2 = strcat(filepath, name, '_T1-list_siexp_no-outlier.xlsx');
        writetable(S,docnam2)
        maxhis_si = max(T1Sno);
        meanhis_si = mean(T1Sno);
        if maxhis_si > (meanhis_si*5)
            maxhis_si = meanhis_si*5;
        end
        [~, density, xmesh, cdf] = kde(T1Sno, 2^12, 0, maxhis_si);
        x = find(density == max(density), 1, 'first');
        maxx_si = xmesh(x);
        A = [xmesh', density];
        writetable(table(A), char(strcat(filepath, name, '_siexp_kde_no-outlier.xlsx')));
        min_si = find(cdf >= min95, 1, 'first');
        min95_si = xmesh(min_si);
        max_si = find(cdf >= max95, 1, 'first');
        max95_si = xmesh(max_si);
        mod_si_no = [maxx_si min95_si max95_si];
        writetable(table(mod_si_no, 'VariableNames', {'T1_mode'}), char(strcat(filepath, name, '_siexp_kde_mode_no-outlier.xlsx')))
        clear lbo ubo T1Sno S docnam2 maxhis_si meanhis_si density xmesh cdf x maxx_si A min_si min95_si max_si max95_si mod_si_no
    end
    if strcmp(model, 'srexp') || strcmp(model, 'all')
        lbo = prctile(T1R, 25) - 1.5*iqr(T1R);
        ubo = prctile(T1R, 75) + 1.5*iqr(T1R);
        T1Rno = T1R(T1R > lbo & T1R < ubo);
        S = table(T1Rno, 'VariableNames', {'T1_srexp'});
        docnam2 = strcat(filepath, name, '_T1-list_srexp_no-outlier.xlsx');
        writetable(S,docnam2)
        maxhis_sr = max(T1Rno);
        meanhis_sr = mean(T1Rno);
        if maxhis_sr > (meanhis_sr*5)
            maxhis_sr = meanhis_sr*5;
        end
        [~, density, xmesh, cdf] = kde(T1Rno, 2^12, 0, maxhis_sr);
        x = find(density == max(density), 1, 'first');
        maxx_sr = xmesh(x);
        A = [xmesh', density];
        writetable(table(A), char(strcat(filepath, name, '_srexp_kde_no-outlier.xlsx')))
        min_sr = find(cdf >= min95, 1, 'first');
        min95_sr = xmesh(min_sr);
        max_sr = find(cdf >= max95, 1, 'first');
        max95_sr = xmesh(max_sr);
        mod_sr_no = [maxx_sr min95_sr max95_sr];
        writetable(table(mod_sr_no, 'VariableNames', {'T1_mode'}), char(strcat(filepath, name, '_srexp_kde_mode_no-outlier.xlsx')))
        clear lbo ubo T1Rno S docnam2 maxhis_sr meahis_sr density xmesh cdf x maxx_sr A min_sr min95_sr max_sr max95_sr mod_sr_no
    end
end

    

%clear T bootfile bootsub timetags triggers bootsub tmp bootfile T boot

end