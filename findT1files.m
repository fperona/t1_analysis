function [files, nf] = findT1files(folder)
    fileList = dir(fullfile(folder,'**','*.t1'));
    nf = length(fileList); % quantity of the T1 files in the folders
    files = strings(nf, 1);
    for i=1:nf
        files(i) = fullfile(fileList(i).folder, fileList(i).name); % build the complete file path
    end
end