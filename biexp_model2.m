function [y_fitted, params, y_TC, QA_fit, cilb, ciub, AdjR2] = biexp_model2(dt, y, varargin)
    ylow = min(y);
    yhigh = max(y);
    contrast = (yhigh - ylow);
    mean_int = contrast/2 + ylow;
    med_dt = median(dt);
    
    % Parameters
    MinAdjR2 = 0.93; % Minimum value of the adjusted coefficient of determination (R^2) accepted to approve the fitting.
    MinTc = min(dt);
    MaxTc = max(dt);

    % Initial values
    % Get the initial values if there were given to the function
    n_arg = length(varargin);
    if n_arg == 0
        x0 = [mean_int, med_dt, mean_int, 0.1*med_dt];
        % disp('Default x0')
    else
        x0 = [varargin{:}];
    end
    
    % Parameters to find
    % b = A1
    % c = Ta
    % d = A2
    % e = Tb
    % Function model
    F = '1 + b*exp(-x/c) + d*exp(-x/e)';

    % Parameters boundaries
    lb = [1e-2, MinTc, 1e-2, MinTc];
    ub = [contrast , MaxTc, contrast , MaxTc];

    % Fitting
    options = fitoptions('Method', 'NonlinearLeastSquares');
    options.Lower = lb;
    options.Upper = ub;
    options.TolFun = 1e-9;
    options.MaxIter = 1e4;
    options.MaxFunEvals = 1e4;
    options.TolX = 1e-9;
    options.Startpoint = x0;
    [fci, gof, output] = fit(dt',y',F,options);
    params = coeffvalues(fci);

    % Choose the slower time constant
    TC = max(params([2 4]));

    % Analyse the fitting quality
    resnorm = gof.sse;
    ci = confint(fci);
    cilb = ci(1,:);
    ciub = ci(2,:);
    AdjR2 = gof.adjrsquare;
    QA_fit = true;
    if (AdjR2 < MinAdjR2)
        fprintf(2,'Adj. R^2 too low ')
        QA_fit = false;
    end
    if (TC <= 10 || TC >= 1e3) 
        if ~QA_fit; fprintf('| '); end
        fprintf(2,'T1 out of range ')
        QA_fit = false;
    end
    if (output.exitflag <= 0)
        if ~QA_fit; fprintf('| '); end
        fprintf(2,'Fitting FAILED ')
        QA_fit = false;
    end
    if QA_fit; s="OK"; else; s=" "; end
    fprintf("%s \n", s);
        
    % Choose the slower time constant and build the fitted curve
    y_TC = feval(fci,TC);
    y_fitted = feval(fci,dt);

    % Sort the parameters so the slower constant is in the fourth position
    if params(2) > params(4)
        tmp = [params(1) params(2)];
        params([1 2]) = [params(3) params(4)];
        params([3 4]) = tmp;
        tmplb = [cilb(1) cilb(2)];
        tmpub = [ciub(1) ciub(2)];
        cilb([1 2]) = [cilb(3) cilb(4)];
        ciub([1 2]) = [ciub(3) ciub(4)];
        cilb([3 4]) = tmplb;
        ciub([3 4]) = tmpub;
    end
end
   
