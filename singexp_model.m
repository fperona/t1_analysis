function [y_fitted, params, y_TC, QA_fit, cilb, ciub, AdjR2] = singexp_model(dt, y, varargin)
    ylow = min(y);
    yhigh = max(y);
    contrast = (yhigh - ylow);
    mean_int = contrast/2 + ylow;
    med_dt = median(dt);
    
    % Parameters
    MinAdjR2 = 0.93; % Minimum value of the adjusted coefficient of determination (R^2) accepted to approve the fitting.
    MinTc = min(dt);
    MaxTc = max(dt);

    % Initial values
    % Get the initial values if there were given to the function
    n_arg = length(varargin);
    if n_arg == 0
        x0 = [mean_int, med_dt];
    else
        x0 = [varargin{:}];
    end
    
    % Parameters to find
    % x(1) = A
    % x(2) = tc
   
    % Function model
    % F = @(x,dt) 1 + x(1)*exp(-dt/x(2));
    F = '1 + A*exp(-x/tc)';

    % Parameters boundaries
    lb = [1e-2, MinTc];
    ub = [contrast , MaxTc];

    % Fitting
    % options = optimoptions('lsqcurvefit','FunctionTolerance',1e-9,'MaxIterations',1e4,'MaxFunctionEvaluations',1e4,'StepTolerance',1e-9);
    % [params,resnorm,~,~,~] = lsqcurvefit(F,x0,dt,y,lb,ub,options);
    options = fitoptions('Method', 'NonlinearLeastSquares');
    options.Lower = lb;
    options.Upper = ub;
    options.TolFun = 1e-9;
    options.MaxIter = 1e4;
    options.MaxFunEvals = 1e4;
    options.TolX = 1e-9;
    options.Startpoint = x0;
    [fci, gof, output] = fit(dt',y',F,options);
    params = coeffvalues(fci);
    
    % Choose the time constant
    TC = params(2);

    % Analyse the fitting quality
    resnorm = gof.sse;
    ci = confint(fci);
    cilb = ci(1,:);
    ciub = ci(2,:);
    AdjR2 = gof.adjrsquare;
    QA_fit = true;
    if (AdjR2 < MinAdjR2)
        fprintf(2,'Adj. R^2 too low ')
        QA_fit = false;
    end
    if (TC <= 10 || TC >= 1e3) 
        if ~QA_fit; fprintf('| '); end
        fprintf(2,'T1 out of range ')
        QA_fit = false;
    end
    if (output.exitflag <= 0)
        if ~QA_fit; fprintf('| '); end
        fprintf(2,'Fitting FAILED ')
        QA_fit = false;
    end
    if QA_fit; s="OK"; else; s=" "; end
    fprintf("%s \n", s);

    % Build the fitted curve
    y_TC = feval(fci,TC);
    y_fitted = feval(fci,dt);

    

end
   
