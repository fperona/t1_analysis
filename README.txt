Matlab T1_Analysis script

The script is used to help analyze the data recorder in the T1 magnetometers developed in the Bioimaging & Bioanalysis group.

Matlab Toolboxes dependencies:
- Parallel Computing Toolbox (Bootstrap section)
- Optimization Toolbox
- Curve Fitting Toolbox

-------------------------------------------------------
How to use:
Basic
- Open the script T1_Analysis.m in Matlab.
- Write the path to the folder containing your data in the variable 'folder' (third line).
- Run the script. You can do that by clicking the green triangle in the manubar or writing 'T1_Analysis' in the console.
- The script will take time to read all the files and calculate the relaxation times. Wait until there are no more new outputs in the console.
- The results are saved in an Excel file named 'Results_T1_biexponential.xlsx' in the same folder as your data.
- You can change the fitting model to a single exponential by refining the parameter MODEL to 'exponential'. In this case, the name of the Excel file will be 'Results_T1_exponential.xlsx'.
- Before processing another folder, clean the memory and outputs by typing in the Command Window:
  clear; clc

Advance
- In addition to the Excel file, the results (and many more details) are stored in a variable named T1_experiment. This variable is an array of objects of type T1curve, on which each element contains the information from one T1 file.
- You can use several additional functions to improve the analysis, plot curves, select data, etc. All of them are in the class T1curve.
